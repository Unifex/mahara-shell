# Creating Commands

> phpDocumentor generated [documentation is available](docs/index.html).

## Discovery
Mash will load all the library files it can find and check if they have a `mashGetCommands` method.

There are 2 locations that are checked when looking for classes.

1. `commands/` in this codebase. This is used for commands that don't naturally fall into an existing lib class file. For `newCommand.php` the class would need to be called `mashNewCommand`.
1. Commands are added to the libraries that are exposed via [`plugin_types()`](https://git.mahara.org/mahara/mahara/-/blob/main/htdocs/lib/mahara.php#L1851) from the Mahara installation that the `mash` command finds.

Adding a new command is a matter of adding a number of methods to a class.

## mashGetCommands()
This returns an array of arrays that define commands.
```php
      $commands[] = [
        'title' => string,
        'description' => string,
        'name' => string,
        'method' => string,
        'requires_mahara_command' => boolean,
        'options' => [
          '[option]' => [
            'alias' => string,
            'default' => string,
            'help' => string,
          ],
        ],
      ];
```
* **title**:string<br>
The title that appears in `mash list`
* **description**:string<br>
The description that appears in `mash list`
* **name**:string<br>
The command that is invoked.  i.e. 'backup' in `mash backup`
* **method**:string<br>
The method in this commands class that is invoked when this command is called.
* **requires_mahara_command**:boolean<br>
If set, and if `true`, the full `$this` object will be prepended to the `$args` that are passed into the `method`.
* **options**:string<br>
These are the cli options that are available. `mash` processes these, addes them to `$args` and passes them in to the `method`.
* **options[[option]]**:string<br>
The long form of the option. i.e. `destination` in `mash backup --destination=/path/to/dir`
* **options[[option]][alias]**:string<br>
The short form of the option. i.e. `d` in `mash backup -d /path/to/dir`
* **options[[option]][default]**:string<br>
The default value if there is no value supplied.
* **options[[option]][help]**:string<br>
A description that is presented when command help is requested. i.e.  `mash backup --help`

## Command method
A command needs to be a `public static function` and, while there is no enforcement of this, prefixing the method with `cli` helps with discovery for devs scanning the code.  c.f. `PluginSearchElasticsearch7::cliResetIndex()`

### Passing args to the command
The command must take a single array that contains the parsed commandline parameters and, if configured, `$arg[0]` will be the `MaharaCommand` object. This provides the CLI printing functions, etc.
