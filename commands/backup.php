<?php

use MaharaShell\MaharaCommandBase;
use CliArgs\CliArgs;

/**
 * Backup a Mahara site.
 */
class mashBackup extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'WIP: Backup',
        'description' => 'Create backups for the Mahara database and site files.',
        'name' => 'backup',
        'method' => 'cliBackup',
        'requires_mahara_command' => true,
        'options' => [
          'destination' => [
            'alias' => 'd',
            'default' => '.',
            'help' => 'The destination the backup files will be written to. ' .
              'This will be relative to the current directory. ' .
              'Defaults a randomly named folder in the current directory. ',
          ],
          'skip-db' => [
            'default' => false,
            'help' => '@todo: Skip backing up the database.',
          ],
          'skip-files' => [
            'default' => false,
            'help' => '@todo: Skip backing up the files.',
          ],
        ],
      ];
    }
    return $commands;
  }

  /**
   * Backup a Mahara site.
   *
   * @return void
   */
  public static function cliBackup($args) {
    if (!defined('CLI')) {
      return;
    }
    self::cliPrintH1('Backing up your site.');
    self::cliPrintH2('This is a WIP.');
    self::cliPrint("\n");
    $maharaCommand = $args[0];
    $CliArgs = new CliArgs($maharaCommand->getCommands()['backup']);
    $theseArgs = $CliArgs->getArguments();
    $destinationkey =  $CliArgs->isFlagExist('destination')?'destination':'d';
    $destination = array_key_exists($destinationkey, $theseArgs)
      ? $theseArgs[$destinationkey]
      : './mash-backup-' . date('ymd-His');

    // convert relative paths to absolute paths.
    if (substr($destination, 0, 1) !== '/') {
      $destination = getcwd() . '/' . $destination;
    }

    // Check if the destination is a directory and exists.
    if (!is_dir($destination)) {
      // Create the destination directory.
      mkdir($destination, 0777, true);
    }
    // realpath() requires the directory to exist.
    $destination = realpath($destination);
    // The tidy version of the destination.
    self::cliPrint("Backup Destination: $destination");
    
    if (!$CliArgs->isFlagExist('skip-db')) {
      self::backupDatabase($destination);
    }

    if (!$CliArgs->isFlagExist('skip-files')) {
      // Backup the files.
      mashFixPerms::cliFixPermissions();
      self::backupFiles($destination);
    }
  }

  /**
   * Backup the database.
   *
   * @param string $destination The destination to write the backup to.
   * @return void
   */
  public static function backupDatabase($destination) {
    self::cliPrintH2('Backing up the database');
    self::cliPrint('@TODO: test mysql');

    // Fetch the database configuration
    $dbhost = get_config('dbhost');
    $dbport = get_config('dbport');
    $dbname = get_config('dbname');
    $dbuser = get_config('dbuser');
    $dbpass = get_config('dbpass');
    $dbtype = get_config('dbtype');
    $dbprefix = get_config('dbprefix', '');
    if ($dbtype === 'postgres') {
      // Prefix the command with the pg password environment variable.
      $cmd = "PGPASSWORD=$dbpass pg_dump -h $dbhost";
      if ($dbport) {
        $cmd .= " -p $dbport";
      }
      $cmd .= " -Fc -U $dbuser -d $dbname -f $destination/mash-db-backup.pgdump";
    }
    else if ($dbtype === 'mysql') {
      $cmd = "mysqldump -h $dbhost";
      if ($dbport) {
        $cmd .= " -p $dbport";
      }
      $cmd .= " -u $dbuser -p$dbpass $dbname > $destination/mash-db-backup.sql";
    }
    else {
      self::cliPrint("Unknown database type: $dbtype");
      self::cliExit(1);
    }
    self::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to backup the database.");
    }
  }

  /**
   * Backup the files.
   *
   * @param string $destination The destination to write the backup to.
   * @return void
   */
  public static function backupFiles($destination) {
    self::cliPrintH2('@TODO: Backing up the files');
    // Fetch the site files directory.
    $sitefilesdir = get_config('dataroot');
    $sitedir = basename($sitefilesdir);
    $maharadatadir = dirname($sitefilesdir);
    // Tar up the files.
    $cmd = "tar ";
    // Exclude the dwoo dir
    $cmd .= "--exclude='dwoo' ";
    // Exclude the sessions dir
    $cmd .= "--exclude='sessions' ";
    $cmd .= "-czf $destination/mash-files-backup.tar.gz -C $maharadatadir $sitedir";
    self::cliPrint("Running: $cmd");

    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to backup the files.");
    }
    self::cliPrint("Files backed up to $destination/mash-files-backup.tar.gz");
  }

}