<?php

use MaharaShell\MaharaCommandBase;
use CliArgs\CliArgs;

/**
 * Restore a Mahara site from a backup.
 */
class mashRestore extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'WIP: Restore from backup',
        'description' => 'Connect to the CLI interface of the site database.',
        'name' => 'restore',
        'method' => 'cliRestore',
        'requires_mahara_command' => true,
        'options' => [
          'source' => [
            'alias' => 's',
            'default' => '.',
            'help' => 'The source of the backup to restore from. This can ' .
              'be a file or directory path. The default is the current ' .
              'directory.',
          ],
          'skip-db' => [
            'default' => false,
            'help' => 'Skip restoring the database.',
          ],
          'skip-files' => [
            'default' => false,
            'help' => 'Skip restoring the files.',
          ],
        ],
      ];
    }
    return $commands;
  }

  /**
   * Restore a Mahara site from a backup.
   *
   * @return void
   */
  public static function cliRestore($args) {
    if (!defined('CLI')) {
      return;
    }
    self::cliPrintH1('Restoring site from backup');
    self::cliPrintH2('This is a WIP.');
    self::cliPrint("\n");
    $maharaCommand = $args[0];
    $CliArgs = new CliArgs($maharaCommand->getCommands()['restore']);
    $theseArgs = $CliArgs->getArguments();
    $sourcekey =  $CliArgs->isFlagExist('source')?'source':'s';
    $source = array_key_exists($sourcekey, $theseArgs) ? $theseArgs[$sourcekey] : '.';
    self::cliPrint("Source: $source");

    // Check if the source is a file or directory.
    if (is_file($source)) {
      self::cliPrint("Source is a file.");
    }
    else if (is_dir($source)) {
      self::cliPrint("Source is a directory.");
    }
    else {
      self::cliExit("Source is not a file or directory.");
    }

    // Find the db backup file.
    $dbbackup = self::findDbBackup($source);

    // Find the files backup archive.
    $filesbackup = self::findFilesBackup($source);
    self::cliPrintH2("Source summary");
    self::cliPrint("DB backup: $dbbackup");
    self::cliPrint("Files backup: $filesbackup");
    
    if (!$CliArgs->isFlagExist('skip-db')) {
      // Check the type of database backup we have.
      $dbtype = self::checkDbBackupType($dbbackup);
      if ($dbtype[0] == 'pg') {
        self::cliPrint("Database backup is a PostgreSQL dump.");
        // Restore the database to postgres.
        self::restorePostgres($dbbackup, $dbtype[1]);
      }
      else if ($dbtype[0] == 'mysql') {
        self::cliExit("Don't handle MySQL backups yet.");
      }
      else  if ($dbtype[0] == 'sql'){
        self::cliExit("TODO: Load raw SQL backup into site defined db.");
      }
    }

    if (!$CliArgs->isFlagExist('skip-files')) {
      // Restore the files.
      self::restoreFiles($filesbackup);
    }
  }

  /**
   * Check the type of the database backup.
   *
   * @todo Handle other types of database backups.
   * @param string $dbbackup The path to the database backup file.
   * @return array The target database type and the archive type.
   */
  public static function checkDbBackupType($dbbackup) {

    // Get the file extension.
    $ext = pathinfo($dbbackup, PATHINFO_EXTENSION);

    switch ($ext) {
        
      case 'sql':
        return ['sql', 'none'];

      case 'pg':
        return ['pg', 'none'];

        case 'pgdump':
          return ['pg', 'none'];

      case 'tgz':
        return ['pg', 'tgz'];

      case 'gz':
        return ['pg', 'tgz'];

      case 'bz2':
        return ['pg', 'bz2'];

      case 'zx':
        return ['pg', 'zx'];

      case 'tar':
        return ['pg', 'tar'];
    }
    self::cliExit("Unknown or unhandled database backup file extension: $ext");
  }

  /**
   * Find the database backup file.
   *
   * @param string $source The source of the backup.
   *
   * @return string The path to the database backup file.
   */
  public static function findDbBackup($source) {
    $dbbackup = '';
    if (is_dir($source)) {
      // Find the postgres backup file.
      $dbbackup = self::findFile($source, ['pg_dump', 'pgdump', 'pg', 'sql']);
    }
    return $dbbackup;
  }

  /**
   * Find the files backup archive.
   *
   * @param string $source The source of the backup.
   *
   * @return string The path to the files backup archive.
   */
  public static function findFilesBackup($source) {
    $filesbackup = '';
    if (is_dir($source)) {
      // Find the files backup archive.
      $filesbackup = self::findFile($source, ['tar', 'tgz', 'gz', 'xz', 'zip']);
    }
    return $filesbackup;
  }

  /**
   * Find a file in a directory.
   *
   * @param string $dir The directory to search.
   * @param array $extensions The file extensions to search for.
   *
   * @return string The path to the file.
   */
  public static function findFile($dir, $extensions) {
    $file = '';
    $files = scandir($dir);
    foreach ($files as $filename) {
      $path = $dir . '/' . $filename;
      if (is_file($path)) {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if (in_array($ext, $extensions)) {
          $file = $path;
          break;
        }
      }
    }
    return $file;
  }

  /**
   * Get the file extension of a file.
   *
   * If the file is a tar archive return the 'tar' as well.
   *
   * @param string $file The file to get the extension of.
   * @return string The file extension.
   */
  public static function getFileExtension($file) {
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    $filename = pathinfo($file, PATHINFO_FILENAME);
    // Check the second extension.
    if (pathinfo($filename, PATHINFO_EXTENSION) == 'tar') {
      $ext = 'tar.' . $ext;
    }
    return $ext;
  }

  /**
   * Return the tar extraction flags for a file extension.
   *
   * @param string $ext The file extension.
   * @return string The tar extraction flags.
   */
  public static function getTarFlags($ext) {
    switch ($ext) {
      case 'tar':
        return 'xvf';

      case 'tar.gz':
        return 'xvzf';

      case 'tar.bz2':
        return 'xvjf';

      case 'tar.xz':
        return 'xvJf';

      case 'tgz':
        return 'xvzf';

      case 'gz':
        return 'xvzf';

      case 'bz2':
        return 'xvjf';

      case 'xz':
        return 'xvJf';

      case 'zip':
        return 'xvf';
    }
    self::cliExit("Unknown or unhandled file extension: $ext");
  }

  /**
   * Restore a PostgreSQL database backup.
   *
   * @param string $dbbackup The path to the database backup file.
   * @param string $archive The type of archive.
   *
   * @return void
   */
  public static function restorePostgres($dbbackup, $archive) {
    $dbhost = get_config('dbhost');
    $dbport = get_config('dbport');
    $dbname = get_config('dbname');
    $dbuser = get_config('dbuser');
    $dbpass = get_config('dbpass');
    $dbtype = get_config('dbtype');
    $dbprefix = get_config('dbprefix', '');
    // Sanity check the db type.
    if ($dbtype != 'postgres') {
      self::cliExit("Database type for the site is not PostgreSQL.");
    }
    // Check the database backup file is readable.
    if (!is_readable($dbbackup)) {
      self::cliExit("Database backup file is not readable: $dbbackup");
    }

    // Drop the database
    self::cliPrint("Dropping the database.");
    // Ensure all users are disconnected.
    $sql = "SELECT pg_terminate_backend(pg_stat_activity.pid)
      FROM pg_stat_activity
      WHERE pg_stat_activity.datname = '$dbname'
      AND pid <> pg_backend_pid();";
    $cmd = "sudo -u postgres psql -c \"$sql\"";
    self::cliPrint("Disconnecting users: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to disconnect users.");
    }
    // Drop the database as the postgres user.
    $cmd = "sudo -u postgres dropdb --if-exists $dbname";
    
    self::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to drop all tables in the database.");
    }

    // Create the database.
    self::cliPrint("Creating the database.");
    // $cmd = "PGPASSWORD=$dbpass psql -h $dbhost -p $dbport -U $dbuser -c 'CREATE DATABASE $dbname;'";
    $cmd = "sudo -u postgres createdb -O$dbuser $dbname";
    self::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to create the database.");
    }

    // Restore the database.
    self::cliPrint("Restoring the database.");
    // $cmd = "PGPASSWORD=$dbpass pg_restore -h $dbhost -p $dbport -U $dbuser -d $dbname $dbbackup";
    $cmd = "PGPASSWORD=$dbpass pg_restore -O -j4 -d $dbname -U $dbuser -h $dbhost $dbbackup";
    self::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliPrint("Errors were reported. See the above messages.");
    }

    // Sanity check some db values.
    $sql = "UPDATE ${dbprefix}config SET value = '' WHERE field = 'wwwroot';";
    $cmd = "PGPASSWORD=$dbpass  psql -h $dbhost " ;
    if ($dbport) {
      $cmd .= " -p $dbport";
    }
    $cmd .= " -U $dbuser -d $dbname -c \"$sql\"";
    self::cliPrint("Setting wwwroot to empty: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to update config.");
    }
  }

  /**
   * Restore a files backup archive.
   *
   * @param string $filesbackup The path to the files backup archive.
   * @param string $archive The type of archive.
   *
   * @return void
   */
  public static function restoreFiles($filesbackup) {
    $dataroot = get_config('dataroot');
    // Strip the trailing slash for chmod cmds etc.
    $printdataroot = rtrim($dataroot, '/');
    $ext = self::getFileExtension($filesbackup);
    // Fetch the extraction flags for the archive type.
    $flags = self::getTarFlags($ext);

    self::cliPrint("Restoring $ext archive $filesbackup to $dataroot");
    // Check the files backup archive is readable.
    if (!is_readable($filesbackup)) {
      self::cliExit("Files backup archive is not readable: $filesbackup");
    }
    // Ensure we can act on the data root.
    mashFixPerms::cliFixPermissions();
    // Remove the files directory.
    if (is_dir($dataroot)) {
      self::cliPrint("Removing the files from $dataroot");
      $cmd = "rm -rf $dataroot/*";
      self::cliPrint("Running: $cmd");
      $output = [];
      $return = 0;
      exec($cmd, $output, $return);
      if ($return != 0) {
        self::cliPrint("Something went wrong");
        self::cliPrint("Try this: mash fixperms");
        self::cliPrintWarning("Ensure you understand the above command before running it in a non-development environment.");
        self::cliExit("Failed to remove the files directory.");
      }
    }
    // Restore the files.
    self::cliPrint("Restoring the files.");
    $cmd = "tar -C $dataroot -$flags $filesbackup";
    self::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      self::cliExit("Failed to restore the files.");
    }

    // If there is only one directory in our extracted files, move it up.
    $files = scandir($dataroot);
    if (count($files) == 3) {
      self::cliPrint("We have a single directory at the top of the files archive.");
      self::cliPrint("Moving the files up one directory.");
      $dir = $files[2];
      $cmd = "mv $dataroot/$dir/* $dataroot";
      self::cliPrint("Running: $cmd");
      $output = [];
      $return = 0;
      exec($cmd, $output, $return);
      if ($return != 0) {
        self::cliExit("Failed to move the files.");
      }
      $cmd = "rmdir $dataroot/$dir";
      self::cliPrint("Running: $cmd");
      $output = [];
      $return = 0;
      exec($cmd, $output, $return);
      if ($return != 0) {
        self::cliExit("Failed to remove the directory.");
      }
    }

    // Dev assistance notice.
    self::cliPrint("If you are in a development environment this may help here:");
    self::cliPrint("Try this: mash fixperms");
    mashFixPerms::cliFixPermissions();
  }
}