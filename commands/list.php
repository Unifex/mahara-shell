<?php

use MaharaShell\MaharaCommandBase;

/**
 * List commands that are available.
 */
class mashList extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'List available commands',
        'description' => 'Lists the commands that we know about.',
        'name' => 'list',
        'method' => 'cliListCommands',
        'requires_mahara_command' => true,
        'shortname' => 'ls',
      ];
    }
    return $commands;
  }

  /**
   * A callback that lists the available commands.
   *
   * @return void
   */
  public static function cliListCommands($args) {
    if (!defined('CLI')) {
      return;
    }
    $maharaCommand = $args[0];
    $maharaCommand->fullCommandSummary();
    self::cliExit('');
  }
}