<?php

use MaharaShell\MaharaCommandBase;

/**
 * Close or open the site.
 */
class mashCloseSite extends MaharaCommandBase
{

  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'Close site',
        'description' => get_string('closesite', 'admin'),
        'name' => 'close-site',
        'shortname' => 'cs',
        'method' => 'cliCloseSite',
      ];
      $commands[] = [
        'title' => 'Open site',
        'description' => get_string('reopensite', 'admin'),
        'name' => 'open-site',
        'shortname' => 'os',
        'method' => 'cliOpenSite',
      ];
    }
    return $commands;
  }

  /**
   * A callback that closes the Mahara site.
   *
   * @return void
   */
  public static function cliCloseSite()
  {
    if (!defined('CLI')) {
      return;
    }
    set_config('siteclosedbyadmin', 1);
    require_once(get_config('docroot') . 'auth/session.php');
    remove_all_sessions();
    self::cliExit(get_string('cli_close_site_siteclosed', 'admin'));
  }

  /**
   * A callback that opens the Mahara site.
   *
   * @return void
   */
  public static function cliOpenSite() {
    if (!defined('CLI')) {
      return;
    }
    set_config('siteclosedbyadmin', 0);
    self::cliExit(get_string('cli_close_site_siteopen', 'admin'));
  }
}
