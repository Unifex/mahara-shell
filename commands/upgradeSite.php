<?php

use MaharaShell\MaharaCommandBase;

/**
 * Close or open the site.
 */
class mashUpgradeSite extends MaharaCommandBase
{

  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'Upgrade site',
        'description' => get_string('cli_upgrade_description', 'admin'),
        'name' => 'upgrade-site',
        'shortname' => 'up',
        'method' => 'cliUpgradeSite',
      ];
    }
    return $commands;
  }

  /**
   * A callback that closes the Mahara site.
   *
   * @return void
   */
  public static function cliUpgradeSite()
  {
    if (!defined('CLI')) {
      return;
    }

    // Check whether Mahara is installed yet
    if (!table_exists(new XMLDBTable('config'))) {
      self::cliExit(get_string('maharanotinstalled', 'admin'), false);
    }

    $upgrades = check_upgrades();
    if (empty($upgrades['settings']['toupgradecount'])) {
      self::cliExit(get_string('noupgrades', 'admin'), false);
    }

    self::cliPrint('---------- begin upgrade ' . date('r', time()) . ' ----------');

    // Check for issues which would pose problems during upgrade
    ensure_upgrade_sanity();

    if (get_field('config', 'value', 'field', '_upgrade')) {
        if ($force) {
            // delete the old flag
            delete_records('config', 'field', '_upgrade');
        }
        else {
            self::cliExit(get_string('cli_upgrade_flag', 'admin'), false);
        }
    }
    // set the flag for this run
    insert_record('config', (object) array('field' => '_upgrade', 'value' => time()));

    // Clear all caches
    clear_all_caches();

    // Actually perform the upgrade
    self::cliPrint(get_string('cli_upgrade_title', 'admin'));
    foreach ($upgrades as $name => $data) {
        // Check to make sure the plugin hasn't already been update out-of-sequence
        if ($name != 'settings' && $newdata = check_upgrades($name)) {
            upgrade_mahara(array($name => $newdata));
        }
    }
    // upgrade completed so remove any upgrade holds
    delete_records('config', 'field', '_upgrade');
    self::cliPrint('---------- upgrade finished ' . date('r', time()) . ' ----------');
  }
}