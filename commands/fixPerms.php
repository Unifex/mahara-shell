<?php

use MaharaShell\MaharaCommandBase;
use CliArgs\CliArgs;

/**
 * Fix permissions on the Mahara filesystem.
 */
class mashFixPerms extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'Fix permissions on the Mahara filesystem',
        'description' => 'Set the permissions to something that will let us manipulate the Mahara filesystem for this site.' .
          " This should only be used on development sites.\n" . 
          ' @TODO: Add the option to set the permissions to something other than the default "a+rwX".',
        'name' => 'fixperms',
        'method' => 'cliFixPermissions',
        'requires_mahara_command' => false,
        // 'options' => [
        //   'destination' => [
        //     'alias' => 'd',
        //     'default' => '.',
        //     'help' => 'The destination the backup files will be written to. ' .
        //       'This will be relative to the current directory. ' .
        //       'Defaults a randomly named folder in the current directory. ',
        //   ],
        // ],
      ];
    }
    return $commands;
  }

  /**
   * Fix the permissions on the Mahara filesystem.
   *
   * @return void
   */
  public static function cliFixPermissions() {
    if (!defined('CLI')) {
      return;
    }
    self::cliPrintH1('Fix Permissions');
    // Get the mahara filesystem root.
    $dataroot = get_config('dataroot');
    self::cliPrint('Mahara root: ' . $dataroot);
    // Set the permissions of $dataroot so that we have all the permissions we need.
    $perms = 'a+rwX';
    self::cliPrint('Setting permissions to: ' . $perms);
    $command = 'sudo chmod -R ' . $perms . ' ' . $dataroot;
    self::cliPrint('Running command: ' . $command);
    $output = [];
    $return = 0;
    exec($command, $output, $return);
    if ($return !== 0) {
      self::cliPrint('There was an error running the command.');
      self::cliPrint('Output: ' . implode("\n", $output));
      self::cliExit('Error: ' . $return);
    }
    self::cliPrint('Done.');
  }

}