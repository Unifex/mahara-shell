<?php

use MaharaShell\MaharaCommandBase;
use CliArgs\CliArgs;

/**
 * List commands that are available.
 */
class mashDb extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'WIP: Open site database',
        'description' => 'Connect to the CLI interface of the site database.',
        'name' => 'db',
        'method' => 'cliDbConnect',
        'requires_mahara_command' => true,
        'options' => [
            'show-command' => [
                'alias' => 's',
                'default' => false,
                'help' => 'Show the command that will be run instead of connecting to the db.'
            ],
            'with-password' => [
                'alias' => 'p',
                'default' => false,
                'help' => 'Include the password in the displayed command if it is an option that can be passed on the CLI.'
            ],
        ],
      ];
    }
    return $commands;
  }

  /**
   * A callback that lists the available commands.
   *
   * @return void
   */
  public static function cliDbConnect($args) {
    if (!defined('CLI')) {
      return;
    }
    $maharaCommand = $args[0];
    $CliArgs = new CliArgs($maharaCommand->getCommands());
    self::cliPrintH1('Opening site database');
    self::cliPrintH2('This is a WIP.');
    self::cliPrint("\n");
    $db = get_config('dbtype');
    $host = get_config('dbhost');
    $port = get_config('dbport');
    $user = get_config('dbuser');
    $pass = get_config('dbpass');
    $name = get_config('dbname');
    if ($db == 'postgres') {
      $cmd = "psql -h $host -p $port -U $user  $name";
      $showCommand = $CliArgs->isFlagExist('show-command') || $CliArgs->isFlagExist('s');
      $withPassword = $CliArgs->isFlagExist('with-password') || $CliArgs->isFlagExist('p');
      if ($showCommand && $withPassword) {
        $cmd = "PGPASSWORD=$pass $cmd";
      }
    }
    else if ($db == 'mysql') {
      MaharaCommandBase::cliPrintWarning('MySQL is currently a WIP. Please report any issues.');
      $cmd = "mysql -h $host -P $port -u $user -p$name";
    }
    else {
      MaharaCommandBase::cliExit("Unsupported database type: $db");
    }

    if ($showCommand) {
      self::cliPrint($cmd);
    }
    else {
      self::cliPrint("Running command: $cmd");
      if ($db == 'postgres') {
        self::cliPrintWarning("This is a WIP. psql does not seem to show its prompts, but you will be on a psql prompt.\n");
        $cmd = "PGPASSWORD=$pass $cmd";
      }
      passthru($cmd);
    }
  }
}