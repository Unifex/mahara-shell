<?php

use MaharaShell\MaharaCommandBase;

/**
 * A Cache Clear command for Mash.
 */
class mashCacheClear extends MaharaCommandBase
{

  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands() {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'Cache Clear',
        'description' => 'Clears caches for templates, resized images, menues, and others.',
        'name' => 'cache-clear',
        'shortname' => 'cc',
        'method' => 'cliCacheClear',
      ];
    }
    return $commands;
  }

  /**
   * A callback that clears the Mahara caches.
   *
   * @return void
   */
  public static function cliCacheClear() {
    if (!defined('CLI')) {
      return;
    }
    mashFixPerms::cliFixPermissions();
    $result = clear_all_caches();
    if ($result) {
      log_info(get_string('clearingcachessucceed', 'admin'));
      self::cliExit(get_string('clearingcachessucceed', 'admin'));
    }
    else {
      self::cliExit("You may want to run this as the webserver user.");
    }
  }
}
