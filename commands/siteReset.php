<?php

use MaharaShell\MaharaCommandBase;
// use CliArgs\CliArgs;

/**
 * Restore a Mahara site from a backup.
 */
class mashSiteReset extends MaharaCommandBase
{
  /**
   * Commands provided by this class.
   *
   * @return array<int,array> The list of commands we provide.
   */
  public static function mashGetCommands()
  {
    $commands = [];
    if (defined('CLI')) {
      $commands[] = [
        'title' => 'Reset a Mahara site',
        'description' => 'Deletes the site data directory and drops and creates the DB. ' .
          "This command uses sudo. You will need to enter your password.\n" .
          '@todo: Add a sanity check step and have the user confirm the site domain being reset.',
        'name' => 'site-reset',
        'shortname' => 'sr',
        'method' => 'cliReset',
        'requires_mahara_command' => true,
      ];
    }
    return $commands;
  }

  /**
   * Restore a Mahara site from a backup.
   *
   * @return void
   */
  public static function cliReset($args) {
    if (!defined('CLI')) {
      return;
    }
    MaharaCommandBase::cliPrintH1('Reset a Mahara site');
    MaharaCommandBase::cliPrintH2('Be sure you know what you are doing.');
    MaharaCommandBase::cliPrint("This command uses sudo. You will need to enter your password.\n\n");
    $dbtype = get_config('dbtype');
    $dbname = get_config('dbname');
    $dataroot = get_config('dataroot');

    MaharaCommandBase::cliPrint("Resetting DB type: $dbtype");
    MaharaCommandBase::cliPrint("Resetting DB name: $dbname");
    MaharaCommandBase::cliPrint("Resetting data root: $dataroot\n\n");

    // Start with resetting the DB.
    switch ($dbtype) {
      case 'postgres':
        mashSiteReset::resetPostgres();
        break;

      default:
        MaharaCommandBase::cliExit("DB type $dbtype is not supported. Reset aborted.");
    }

    // Now reset the data directory.
    mashSiteReset::resetDataDir();

    
  }


  /**
   * Restore a PostgreSQL database backup.
   *
   * @param string $dbbackup The path to the database backup file.
   * @param string $archive The type of archive.
   *
   * @return void
   */
  public static function resetPostgres() {
    $dbhost = get_config('dbhost');
    $dbport = get_config('dbport');
    $dbname = get_config('dbname');
    $dbuser = get_config('dbuser');
    $dbpass = get_config('dbpass');
    $dbtype = get_config('dbtype');
    $dbprefix = get_config('dbprefix', '');
    // Sanity check the db type.
    if ($dbtype != 'postgres') {
      MaharaCommandBase::cliExit("Database type for the site is not PostgreSQL.");
    }

    // Drop the database
    MaharaCommandBase::cliPrint("Dropping the database.");
    // Ensure all users are disconnected.
    $sql = "SELECT pg_terminate_backend(pg_stat_activity.pid)
      FROM pg_stat_activity
      WHERE pg_stat_activity.datname = '$dbname'
      AND pid <> pg_backend_pid();";
    $cmd = "sudo -u postgres psql -c \"$sql\"";
    MaharaCommandBase::cliPrint("Disconnecting users: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      MaharaCommandBase::cliExit("Failed to disconnect users.");
    }
    // Drop the database as the postgres user.
    $cmd = "sudo -u postgres dropdb --if-exists $dbname";
    
    MaharaCommandBase::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      MaharaCommandBase::cliExit("Failed to drop all tables in the database.");
    }

    // Create the database.
    MaharaCommandBase::cliPrint("Creating the database.");
    $cmd = "sudo -u postgres createdb -O$dbuser $dbname";
    MaharaCommandBase::cliPrint("Running: $cmd");
    $output = [];
    $return = 0;
    exec($cmd, $output, $return);
    if ($return != 0) {
      MaharaCommandBase::cliExit("Failed to create the database.");
    }
  }

  /**
   * Restore a files backup archive.
   *
   * @return void
   */
  public static function resetDataDir() {
    $dataroot = get_config('dataroot');
    // Ensure we can act on the data root.
    mashFixPerms::cliFixPermissions();
    // Remove the files directory.
    if (is_dir($dataroot)) {
      MaharaCommandBase::cliPrint("Removing the files from $dataroot");
      $cmd = "rm -rf $dataroot/*";
      MaharaCommandBase::cliPrint("Running: $cmd");
      $output = [];
      $return = 0;
      exec($cmd, $output, $return);
      if ($return != 0) {
        MaharaCommandBase::cliPrint("Something went wrong");
        MaharaCommandBase::cliPrint("Try this: mash fixperms");
        MaharaCommandBase::cliPrintWarning("Ensure you understand the above command before running it in a non-development environment.");
        MaharaCommandBase::cliExit("Failed to remove the files directory.");
      }
    }

    mashFixPerms::cliFixPermissions();
  }
}