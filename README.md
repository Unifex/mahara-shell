# `mash`: the Mahara Shell
Mash is a command line tool that integrates with [Mahara](https://mahara.org). It was inspired by [Drush](https://www.drush.org), the Drupal Shell, and the authors enthusiam for Alan Alda and the TV show M\*A\*S\*H\*.

## The intent
The intent behind `mash` is to allow the various CLI scripts we currently have to have their code moved into the classes that they act upon.  This will put their activity into the context of those classes and allow for easy expansion of capabilities without the need for bespoke scripts.

For anything that doesn't fit into this pattern bespoke scripts that define a class and follow the pattern can be created in the `commands/` directory. These will be discovered automatically and appear in the output from `mash list`.

# Installation

The Mahara Shell (mash) is intended to be installed globally for the user that will use it and it needs to be run from the environment that runs Mahara.  i.e. If you run Mahara in a container/VM you need to install it there.  Do what ever you usually do to get a shell in that environment and do the following from there.

## Composer 2
You will need Composer 2.  Hit the [Download](https://getcomposer.org/download/) page and follow the instructions there. This will give you a `composer.phar` file. This is a self contained php script that can run as any other program does.

> Hint: Move `composer.phar` to a folder in your `$PATH` and rename it to be just `composer`.

### Understanding $PATH
The `$PATH` environment variable is a list of directories that the shell searches through when looking for executable files. When you type a command in the terminal, the shell looks for an executable file with that name in each directory listed in `$PATH`, in order.

If you have a directory in your `$PATH` put `composer` there.  If not:
1. Create the directory. For example, to create a new directory called bin in your home directory, you can run the following command: <br>
`mkdir ~/bin`
1. Add the new directory to your `$PATH` environment variable. You can do this by adding the following line to your `~/.bashrc` file:<br>
`export PATH="$HOME/bin:$PATH"`<br>
This line adds `~/bin` to the beginning of the `$PATH` list, so that any executables in `~/bin` will be found before executables in other directories.
1. After saving the changes to `~/.bashrc`, you need to source the file to apply the changes to the current shell. You can do this by running:<br>
`source ~/.bashrc`

## Mash on gitlab.wgtn.cat-it.co.nz
While in development, *and in the Catalyst gitlab*, you will need to add the following to your global `composer.json` file.
```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.wgtn.cat-it.co.nz/elearning/mahara-shell"
        }
    ],
    "http_basic": {
        "gitlab.wgtn.cat-it.co.nz": {
            "username": "YOUR GITLAB USERNAME",
            "password": "YOUR GITLAB TOKEN"
        }
    }
}

```

You will also need to have an account and an access token to login with.

To set up the token:
* log in to https://gitlab.wgtn.cat-it.co.nz
* Click on Preferences under your Profile
* Click Access Tokens in the User Settings column on the left
* Give your token a name (I used "composer access"), and expiry (I restrict this type to a year) and select `read_api` and `read_repository`.
* Click "Create Personal Access Token" and enter that into your `composer.json` above. Once you leave this screen you will not be able to get that token back. Creating new ones is not a big deal though.

## Install
Once added you can install `mash`. By default Composer will act on the `composer.json` file in the directory it is run from. It will also create the `vendor` directory in that folder.  It is recommended that you install from your `$HOME` dir.
```
cd ~/
composer require mahara/mash:v0.1-BETA
```
Composer packages with an executable will put these in `$HOME/vendor/bin`. Ensure that this directory is in your `$PATH`. See "Understanding $PATH" above. 

With `$HOME/vendor/bin` is in your `$PATH` the `mash` command should now be available to you.  Change into the `htdocs` dir of a Mahara checkout and run `mash list`.

> Any dir below `htdocs/` will work. As long as you are in the directory structure the command should be able to find and bootstrap Mahara.

# dev notes
## v0.1-BETA git tag
After commiting changes move the git tag. I have an alias set up for that. Just invoke `move-tag` from the `mash` repo directory.
```
alias move-tag='cd $HOME/Mahara/mahara-shell; git push origin :refs/tags/v0.1-BETA; git tag -fa -m "moving tag" v0.1-BETA; git push origin main --tags'
```
## Reinstall/update `mash`
```
composer remove mahara/mash && composer require mahara/mash:v0.1-BETA
```
If you want to take a scorched earth approach, delete your vendor directory, your `composer.lock`, and run `composer install` again. This will regenerate the `composer.lock` file and fetch the latest version of all the things in your `composer.json` file.

## Development on `mash`
It's a hack, but it works.  I've been editing the code in the checked out repo and testing like this:
```
cp -r ~/Projects/PD/mahara-shell/* ~/vendor/mahara/mash/ && mash [CMD]
```
This way testing is a simple edit, ALT-tab, up arrow, ENTER, Check the output, rince, repeat.  Once happy with it, commit it, move the git tag, reinstall `mash` and test again.

## Development on `mash` commands
See [CREATINGCOMMANDS.md](CREATINGCOMMANDS.md)