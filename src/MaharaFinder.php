<?php

/**
 * @file
 * Contains \MaharaShell\MaharaFinder.
 */

namespace MaharaShell;

class MaharaFinder {
  /**
   * Mahara web public directory.
   *
   * @var string
   */
  private $maharaRoot;

  /**
   * The root directory of mash.
   *
   * @var string
   */
  private $mashRoot;

  /**
   * The list of core classes to check for commands.
   */
  private $coreClasses = [
    'User' => 'auth/user.php',
  ];

  /**
   * The list of core classes with commands.
   *
   * @var array
   */
  private $commands = [];

  /**
   * Initialize finder.
   *
   * Optionally pass the starting path.
   *
   * @param string|null $start_path
   *   The path to begin the search from.
   *
   * @throws \Exception
   * @todo Make $start_path mandatory in v2.
   */
  public function __construct($start_path = null) {
    // Initialize path variables to false, indicating their locations are
    // not yet known.
    $this->maharaRoot = false;
    $this->mashRoot = false;

    // If a starting path was provided, attempt to locate and set path
    // variables.
    if (!empty($start_path)) {
      // Check if we are in the Mahara root with the htdocs directory.
      if (is_dir($start_path . '/htdocs')) {
        $start_path = $start_path . '/htdocs';
      }
      $this->discoverRoots($start_path);
    }
  }

  /**
   * Get the Mahara root.
   *
   * @return string|bool
   *   The path to the Mahara root, if it was discovered. False otherwise.
   */
  public function getMaharaRoot() {
    return $this->maharaRoot;
  }

  /**
   * Get the mash root.
   *
   * @return string|bool
   *   The path to the root of our code.
   */
  public function getMashRoot() {
    return $this->mashRoot;
  }

  /**
   * Discover all valid paths.
   *
   * @param $start_path
   *   The path to start the search from.
   *
   * @throws \Exception
   */
  protected function discoverRoots($start_path) {
    // We always know where we are.
    $this->mashRoot = dirname(dirname(__FILE__));

    // Since we are discovering, reset all path variables.
    $this->maharaRoot = false;

    foreach (array(true, false) as $follow_symlinks) {
      $path = $start_path;
      if ($follow_symlinks && is_link($path)) {
        $path = realpath($path);
      }

      // Check the start path.
      if ($this->findAndValidateRoots($path)) {
        return;
      }
      else {
        // Move up dir by dir and check each.
        while ($path = $this->shiftPathUp($path)) {
          if ($follow_symlinks && is_link($path)) {
            $path = realpath($path);
          }
          if ($this->findAndValidateRoots($path)) {
            return;
          }
        }
      }
    }
  }

  /**
   * Determine if a valid Mahara root exists.
   *
   * In addition, set any valid path properties if they are found.
   *
   * @param $path
   *   The starting path to search from.
   *
   * @return bool
   *   True if all root was discovered and validated. False otherwise.
   */
  protected function findAndValidateRoots($path) {
    if (!empty($path) && is_dir($path) && file_exists($path . '/lib/mahara.php')) {
      $this->maharaRoot = $path;
    }

    return $this->allPathsDiscovered();
  }

  /**
   * Quickly determine whether or not all paths were discovered.
   *
   * @return bool True if all paths have been discovered, false if one or
   *              more haven't been found.
   */
  protected function allPathsDiscovered() {
    return !empty($this->maharaRoot) && !empty($this->mashRoot);
  }

  /**
   * Returns parent directory.
   *
   * @param string Path to start from
   *
   * @return string|false Parent path of given path or false when $path is
   *                      filesystem root
   */
  private function shiftPathUp($path) {
    $parent = dirname($path);

    return in_array($parent, ['.', $path]) ? false : $parent;
  }

  /**
   * Load commands from core classes.
   */
  public function loadCoreCommands() {
    foreach ($this->coreClasses as $classname => $file) {
      // Require the class file.
      require_once($this->maharaRoot . '/' . $file);
      if (class_exists($classname)) {
        if (method_exists($classname, 'mashGetCommands')) {
          $class_commands = $classname::mashGetCommands();
          foreach ($class_commands as $command) {
            $this->commands[$command['name']] = $command;
            $this->commands[$command['name']]['classname'] = $classname;
          }
        }
      }
    }
  }

  /**
   * Return the array of commands.
   *
   * @return array<string<array<string,mixed>>
   */
  public function getCommands() {
    return $this->commands;
  }

}
