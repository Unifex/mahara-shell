<?php

namespace MaharaShell;

class MaharaCommandBase
{
  // Shell colour codes.
  public static $Black       = "\033[0;30m";
  public static $DarkGray    = "\033[1;30m";
  public static $Red         = "\033[0;31m";
  public static $LightRed    = "\033[1;31m";
  public static $Green       = "\033[0;32m";
  public static $LightGreen  = "\033[1;32m";
  public static $Brown       = "\033[0;33m";
  public static $Orange      = "\033[0;33m";
  public static $Yellow      = "\033[1;33m";
  public static $Blue        = "\033[0;34m";
  public static $BoldBlue    = "\033[1;94m";
  public static $LightBlue   = "\033[1;34m";
  public static $Purple      = "\033[0;35m";
  public static $LightPurple = "\033[1;35m";
  public static $Cyan        = "\033[0;36m";
  public static $LightCyan   = "\033[1;36m";
  public static $LightGray   = "\033[0;37m";
  public static $White       = "\033[1;37m";

  // Bold and Underline
  public static $Bold        = "\033[1m";
  public static $Underline   = "\033[4m";

  // No Colour
  public static $NC          = "\033[0m";

  /**
   * If any files are required, they can be loaded here.
   *
   * It is intended that this would be overridden.
   *
   * @param string $mahara_root The Mahara root directory.
   *
   * @return void
   */
  public static function requiredFiles($mahara_root)
  {
  }

  /**
   * Print out a message formatted for the command line
   *
   * @param string $message The message to output
   * @return void
   */
  public function cliPrint($message = '')
  {
    echo self::$Green . $message . self::$NC . "\n";
  }

  /**
   * Print out a heading formatted for the command line
   *
   * @param string $message The message to output
   * @return void
   */
  public function cliPrintH1($message = '')
  {
    print(self::$BoldBlue . $message . self::$NC . "\n");
  }

  /**
   * Print out a subheading formatted for the command line
   *
   * @param string $message The message to output
   * @return void
   */
  public function cliPrintH2($message = '')
  {
    print(self::$Cyan . $message . self::$NC . "\n");
  }

  /**
   * Print out a warning formatted for the command line
   *
   * @param string $message The message to output
   * @return void
   */
  public function cliPrintWarning($message = '')
  {
    print(self::$Orange . $message . self::$NC . "\n");
  }

  /**
   * Print out an error formatted for the command line
   *
   * @param string $message The message to output
   * @return void
   */
  public function cliPrintError($message = '')
  {
    print(self::$Red . $message . self::$NC . "\n");
  }

  /**
   * Print an exit message and terminate the script.
   *
   * @param string $message
   * @param int|false $error
   * @return void
   */
  public static function cliExit($message = '', $error = false)
  {

    if (!empty($message)) {
      print($message . "\n\n");
    }

    if (is_int($error)) {
      $exitcode = $error;
    } else if ($error === false) {
      $exitcode = 0;
    } else {
      $exitcode = 127;
    }
    exit($exitcode);
  }
}
