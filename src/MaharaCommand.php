<?php

namespace MaharaShell;

use CliArgs\CliArgs;


class MaharaCommand extends MaharaCommandBase {

  /**
   * The full list of discovered commands.See MaharaFinder::loadCoreCommands()
   * @var Array
   */
  private $commands;

  /**
   * The map of aliases to commands.
   *
   * @var Array
   */
  private $command_aliases;

  /**
   * The command we are running.
   *
   * @var string
   */
  private $command;

  /**
   * Any config that the current command has.
   *
   * This is used to set accepted CLI arguments.
   *
   * @var Array
   */
  private $command_config;

  /**
   * The classname the command is in.
   *
   * @var string
   */
  private $command_classname;

  /**
   * The method in the command_classname we will invoke.
   *
   * @var string
   */
  private $command_method;

  /**
   * Is the command alias. i.e. site reset via `mash sr`.
   *
   * @var bool
   */
  private $is_alias = false;

  /**
   * The method in the command_classname we will invoke.
   *
   * @var string
   */
  private $mahara_root;

  /**
   * Bootstrap the command.
   *
   * Parse the commands available and set the command we are running with any
   * config available.
   *
   * @var Array
   */
  function __construct(Array $commands = [], $mahara_root)
  {
    $this->parseCommands($commands);
    $this->setCommand();
    $this->setConfig();
    $this->mahara_root = $mahara_root;
  }

  /**
   * Parse the commands into a usable format.
   *
   * Builds the internal list of commands and references for any command
   * aliases.
   *
   * @param Array $commands The list of commands.
   *
   * @return void
   */
  private function parseCommands(Array $commands) {
    foreach ($commands as $command_name => $config) {
      // @todo Add sanity check on $command_name.
      // @todo Sanity check the $config.
      $this->commands[$command_name] = $config;
      if (array_key_exists('shortname', $config)) {
        $this->command_aliases[$config['shortname']] = $command_name;
      }
    }
  }

  /**
   * Getter for the commands available.
   *
   * @return Array The list of commands.
   */
  function getCommands() {
    return $this->commands;
  }

  /**
   * Set the command we are running.
   *
   * Pulls the command from the CLI, checks it is a valid command, or the alias
   * of a valid command, and sets the command_classname, command_method, and
   * command_config.
   *
   * @return void
   */
  private function setCommand() {
    $args = empty($_SERVER['argv']) ? [] : $_SERVER['argv'];
    if (!empty($args[1])) {
      $command = $args[1];
      if ($this->isValidCommand($command)) {
        if ($this->is_alias) {
          $this->command = $this->command_aliases[$command];

        }
        else {
          $this->command = $command;
        }
        $this->command_classname = $this->commands[$this->command]['classname'];
        $this->command_method = $this->commands[$this->command]['method'];
        if (array_key_exists('options', $this->commands[$this->command])) {
          $this->command_config = $this->commands[$this->command]['options'];
        }
      }
    }
  }

  /**
   * Set config options for the current command.
   */
  private function setConfig() {
    // Config always has help.
    $this->command_config = ['help' => 'h'];
    if (array_key_exists($this->command, $this->commands)) {
      // We have a command.
      $command = $this->commands[$this->command];
      if (array_key_exists('options', $command)) {
        $this->command_config = array_merge($this->command_config, $command['options']);
      }
    }
  }

  private function isValidCommand($command) {
    if (array_key_exists($command, $this->commands)) {
      return true;
    }
    elseif (array_key_exists($command, $this->command_aliases)) {
      $this->is_alias = true;
      return true;
    }
    return false;
  }

  public function fullCommandSummary($this_command = '') {
    if (!empty($this_command)) {
      self::displayCommandHelp($this_command);
    }
    else {
      MaharaCommandBase::cliPrintH1('Mahara Shell');
      MaharaCommandBase::cliPrintH2('Available commands:');
      foreach ($this->commands as $command => $command_config) {
        self::displayCommandSummary($command);
      }
    }
  }

  /**
   * Display a summary line for a command.
   *
   * @param string $command
   * @return void
   */
  public function displayCommandSummary($command) {
    $command_config = $this->commands[$command];

    $cmd_width = 25;

    $msg = '  ' . $command;
    if (array_key_exists('shortname', $command_config)) {
      $msg .= ' (' . $command_config['shortname'] . ')';
    }
    $msg = str_pad($msg, $cmd_width, ' ');
    $msg .= ' ';

    $length = 80 - strlen($msg);

    $summary = explode( "\n", wordwrap($command_config['description'], $length));
    $pad = false;
    foreach ($summary as $line) {
      if ($pad) {
        $msg .= str_repeat(' ', $cmd_width +1);
      }
      else {
        $pad = true;
      }
      $msg .= $line . "\n";
    }

    MaharaCommandBase::cliPrint($msg);
  }

  /**
   * Display the full help for a command.
   *
   * @param string $command
   * @return void
   */
  public function displayCommandHelp($command) {
    $command_config = $this->commands[$command];
    $options = [];
    MaharaCommandBase::cliPrintH1('Mahara Shell: ' . $command_config['title']);
    $msg = "  usage: mash {$command}";
    if (key_exists('alias', $command_config)) {
      $msg .= "|{$command_config['alias']}";
    }

    MaharaCommandBase::cliPrintH2($msg);
    echo "\n";
    MaharaCommandBase::cliPrint('  ' . $command_config['description']);
    if (array_key_exists('options', $command_config)) {
      $cli = new CliArgs($command_config['options']);
      MaharaCommandBase::cliPrint($cli->getHelp());
    }
  }

  /**
   * Does the command require MaharaCommand ($this)?
   *
   * @return bool
   */
  private function requiresMaharaCommand() {
    if (array_key_exists('requires_mahara_command', $this->commands[$this->command])) {
      return $this->commands[$this->command]['requires_mahara_command'];
    }
    return false;
  }

  /**
   * Return the command arguments as an array.
   *
   * If the arguments have been populated on the cli these values would be
   * added here as well.
   *
   * @return array
   */
  private function getCommandArguments() {
    $CliArgs = new CliArgs($this->commands);
    $cliparams = $CliArgs->getArguments();
    $args = [];
    foreach ($this->command_config as $key => $config) {
      // Get the default if one is set.
      $default = isset($config['default']) ? $config['default'] : '';
      $isbool = isset($config['default']) && is_bool($config['default']);
      // Get the value from the command line if it exists and default to
      // $default if not set.
      if ($isbool) {
        $args[$key] = array_key_exists($key, $cliparams) ? true : $default;
      }
      else {
        $args[$key] = isset($cliparams[$key]) ? $cliparams[$key] : $default;
      }

      // Check if the command alias has been used.
      $alias = false;
      if (is_string($config)) {
        $alias = $config;
      }
      else if (isset($config['alias']) && is_string($config['alias'])) {
        $alias = $config['alias'];
      }
      else if (isset($config['alias']) && is_array($config['alias'])) {
        // Check if the alias exists in the cliparams.
        foreach ($config['alias'] as $a) {
          if (isset($cliparams[$a])) {
            $alias = $a;
            break;
          }
        }
      }
      if ($alias && array_key_exists($alias, $cliparams)) {
        if ($isbool) {
          $args[$key] = true;
        }
        else {
          $args[$key] = $cliparams[$alias];
        }
      }
    }
    return $args;
  }

  /**
   * Run the command.
   */
  public function run() {
    $CliArgs = new CliArgs($this->commands);
    $args = $CliArgs->getArgs();
    // Call our command.
    if (!empty($this->command)) {
      if ($CliArgs->isFlagExist('help') || $CliArgs->isFlagExist('h')) {
        $this->fullCommandSummary($this->command);
      }
      else {
        // Add the cli arguments for the command to the $args.
        $args['command-arguments'] = $this->getCommandArguments();

        // If the command_classname has the requiredFiles method, run it.
        if (method_exists($this->command_classname, 'requiredFiles')) {
          $this->command_classname::requiredFiles($this->mahara_root);
        }
        if ($this->requiresMaharaCommand()) {
          array_unshift($args, $this);
        }

        $this->command_classname::{$this->command_method}($args);
      }
    }
    else {
      MaharaCommandBase::cliPrintH1('Mahara Shell');
      if (count($CliArgs->getArgs()) > 1) {
        MaharaCommandBase::cliPrintH2('No command specified.');
      }
      else {
        MaharaCommandBase::cliPrintH2('Invalid command specified.');
      }
      if ($CliArgs->isFlagExist('help') || $CliArgs->isFlagExist('h')) {
        MaharaCommandBase::cliPrint('The help command requires that a command name be provided.');
      }
      MaharaCommandBase::cliExit('Run `mash list` to see a list of available commands.');
    }
  }
}