<?php

/**
 * @file
 * Contains \MaharaShell\PluginFinder.
 */

namespace MaharaShell;

class PluginFinder
{

  /**
   * The list of plugins we will be checking.
   *
   * @var array
   */
  private $plugins = [];

  /**
   * The list of plugins with commands.
   *
   * @var array
   */
  private $commands = [];

  /**
   * The MaharaFinder.
   * @var MaharaFinder
   */
  private $dirs;

  /**
   * Initialize finder.
   *
   * @param string|null $start_path
   *   The path to begin the search from.
   *
   * @throws \Exception
   * @todo Make $start_path mandatory in v2.
   */
  public function __construct(MaharaFinder $maharaFinder)
  {
    $this->dirs = $maharaFinder;
    $this->loadMashCommands();
    $this->loadMaharaCommands();
  }

  /**
   * Load bundled commands that come with MASH.
   *
   * @return void
   */
  private function loadMashCommands()
  {
    // Load any stand-alone commands.
    $commands_dir = $this->dirs->getMashRoot() . '/commands/';

    $dirhandle = opendir($commands_dir);
    while (false !== ($file = readdir($dirhandle))) {
      if (strpos($file, '.') === 0 or 'CVS' == $file) {
        continue;
      }

      require_once($commands_dir . $file);
      $bits = explode('.', $file);
      $classname = 'mash' . ucfirst($bits[0]);
      if (method_exists($classname, 'mashGetCommands')) {
        $class_commands = $classname::mashGetCommands();
        foreach ($class_commands as $command) {
          $this->commands[$command['name']] = $command;
          $this->commands[$command['name']]['classname'] = $classname;
        }
      }
    }
  }

  /**
   * Examine plugins in Mahara for Mash commands.
   *
   * @return void
   */
  private function loadMaharaCommands()
  {

    // Load all available plugins.
    $pluginstocheck = \plugin_types();
    $mahara_root = $this->dirs->getMaharaRoot();
    foreach ($pluginstocheck as $plugin) {
      $dirhandle = opendir($mahara_root . '/' .  $plugin);
      while (false !== ($dir = readdir($dirhandle))) {
        if (strpos($dir, '.') === 0 or 'CVS' == $dir) {
          continue;
        }
        $plugin_dir = $mahara_root . '/' .  $plugin . '/' . $dir;
        if (!is_dir($plugin_dir)) {
          continue;
        }
        try {
          \validate_plugin($plugin, $dir);
          $this->plugins[] = array($plugin, $dir);
        } catch (InstallationException $_e) {
          \log_warn(\get_string('pluginnotinstallable', 'mahara', $plugin, $dir) . $_e->GetMessage(), true, false);
        }

        if ($plugin == 'artefact') {
          // Check them for the blocks as well.
          $blockttypelocation = $plugin_dir . '/blocktype';
          if (!is_dir($blockttypelocation)) {
            continue;
          }
          $blocktypedirhandle = opendir($blockttypelocation);
          while (false !== ($blocktypedir = readdir($blocktypedirhandle))) {
            if (strpos($blocktypedir, '.') === 0 or 'CVS' == $blocktypedir) {
              continue;
            }
            if (!is_dir($mahara_root . '/' .  $plugin . '/' . $dir . '/blocktype/' . $blocktypedir)) {
              continue;
            }
            $this->plugins[] = array('blocktype', $dir . '/' . $blocktypedir);
          }
        }
      }
    }

    // Check plugins for CLI commands.
    foreach ($this->plugins as $plugin) {
      $plugintype = $plugin[0];
      $pluginname = $plugin[1];
      $pluginpath = "$plugin[0]/$plugin[1]";
      $pluginkey  = "$plugin[0].$plugin[1]";

      if ($plugintype == 'blocktype' && strpos($pluginname, '/') !== false) {
        // sigh.. these are a bit special...
        $bits = explode('/', $pluginname);
        $pluginpath = 'artefact/' . $bits[0] . '/blocktype/' . $bits[1];
      }
      $classname = \generate_class_name($plugintype, $pluginname);
      \safe_require($plugintype, $pluginname);
      // print_r([$plugintype, $pluginname,$pluginpath,$pluginkey]);
      if (method_exists($classname, 'mashGetCommands')) {
        $class_commands = $classname::mashGetCommands();
        foreach ($class_commands as $command) {
          $this->commands[$command['name']] = $command;
          $this->commands[$command['name']]['classname'] = $classname;
        }
      }
    }
  }

  /**
   * Return the array of commands.
   *
   * @return array<string<array<string,mixed>>
   */
  public function getCommands() {
    return $this->commands;
  }


  /**
   * Convert a time to something more human readable.
   *
   * @param int $time
   * 
   * @return string
   */
  function humanTiming($time)
  {
    $time = time() - $time; // to get the time since that moment
    $time = ($time < 1) ? 1 : $time;
    $tokens = array(
      31536000 => 'year',
      2592000 => 'month',
      604800 => 'week',
      86400 => 'day',
      3600 => 'hour',
      60 => 'minute',
      1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
      if ($time < $unit) continue;
      $numberOfUnits = floor($time / $unit);
      return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
    }
  }
}
